//
// Created by newbyte on 4/29/19.
//

#include <iostream>
#include "MazeGenerator.h"

MazeGenerator::MazeGenerator(MazeManager* mazeManager) {
    _mazeManager = mazeManager;
}

void MazeGenerator::generate() {
    _currentCell.x = 0;
    _currentCell.y = 0;

    _mazeManager->setVisited(_currentCell);
    _stack.push_back(_currentCell);

    while (!_stack.empty()) {
        if (_mazeManager->hasUnvisitedNeighbours(_currentCell)) {
            Coordinate neighbourCell = _mazeManager->getRandomNeighbour(_currentCell);

            _mazeManager->makeHoleBetween(_currentCell, neighbourCell);

            _mazeManager->setVisited(neighbourCell);

            _stack.push_back(neighbourCell);

            _currentCell = neighbourCell;
        } else {
            // std::vector does not return the popped item, so
            // this is split into two lines
            _currentCell = _stack.back();
            _stack.pop_back();
        }
    }
}
