//
// Created by neboula on 2019-05-11.
//

#include <algorithm>
#include "WindowManager.h"

WindowManager::WindowManager(const std::string& title, int x, int y, int width, int height) {
    _window = SDL_CreateWindow(title.c_str(), x, y, width, height, SDL_WINDOW_FULLSCREEN);
    _renderer = SDL_CreateRenderer(_window, -1, 0);
    updateVariables();
}

const int WindowManager::getWidth() const {
    return _width;
}

const int WindowManager::getHeight() const {
    return _height;
}

void WindowManager::updateVariables() {
    SDL_GetWindowSize(_window, &_width, &_height);
}

WindowManager::~WindowManager() {
    SDL_DestroyRenderer(_renderer);
    SDL_DestroyWindow(_window);
}

void WindowManager::drawWall(float distance, int width, int x) {
    unsigned char correctedDistance =
            static_cast<unsigned char>(
                    std::clamp(
                            255 - distance * 12,
                            0.0f,
                            255.0f));

    int ceilingHeight =
            static_cast<int>(
                    std::clamp(
                            (distance / 40.0) * getHeight(),
                            0.0,
                            getHeight() / 2.0));

    int wallHeight = std::clamp(getHeight() - ceilingHeight * 2, 0, getHeight());

    SDL_Rect wall;
    wall.w = width;
    wall.h = wallHeight;
    wall.x = x;
    wall.y = ceilingHeight;

    SDL_SetRenderDrawColor(_renderer, correctedDistance, 0, 0, 255);

    if (wallHeight > 0) {
        SDL_RenderDrawRect(_renderer, &wall);
    }

    SDL_Rect floor;
    floor.w = width;
    floor.h = getHeight() / 2 - wallHeight / 2;
    floor.x = x;
    floor.y = ceilingHeight + wallHeight;

    SDL_SetRenderDrawColor(_renderer, 70, 70, 70, 255);

    SDL_RenderFillRect(_renderer, &floor);
}

SDL_Renderer *WindowManager::getRenderer() {
    return _renderer;
}
