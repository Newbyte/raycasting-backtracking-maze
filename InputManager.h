//
// Created by neboula on 2019-05-09.
//

#ifndef SDL2_PIXEL_INPUTMANAGER_H
#define SDL2_PIXEL_INPUTMANAGER_H


#include <SDL2/SDL_keycode.h>
#include <set>

class InputManager {
public:
    void checkInput();
    const bool shouldQuit();
    const std::set<SDL_Keycode>* getPressedKeys();
private:
    std::set<SDL_Keycode> _pressedKeys;
    bool _shouldExit = false;
};


#endif //SDL2_PIXEL_INPUTMANAGER_H
