//
// Created by newbyte on 4/29/19.
//

#ifndef BACKTRACK_MAZE_MAZEGENERATOR_H
#define BACKTRACK_MAZE_MAZEGENERATOR_H

#include "MazeManager.h"
#include "MazeElement.h"
#include "Direction.h"
#include "Coordinate.h"
#include <set>

class MazeGenerator {
public:
    explicit MazeGenerator(MazeManager* mazeManager);
    void generate();
private:
    Coordinate _currentCell;
    MazeManager* _mazeManager;
    std::vector<Coordinate> _stack;
};


#endif //BACKTRACK_MAZE_MAZEGENERATOR_H
