//
// Created by neboula on 2019-04-28.
//

#include "MazeManager.h"
#include <random>
#include <iostream>

MazeManager::MazeManager() {
    setupTemplate(10, 10);

    resetDirections();
}

void MazeManager::buildMaze() {
    _generatedMaze.resize(_templateMaze.size() * 3);

    for (auto& mazeColumn : _generatedMaze) {
        mazeColumn.resize(_templateMaze[0].size() * 3 + 2);

        for (auto& mazeElement : mazeColumn) {
            mazeElement = ' ';
        }
    }

    for (unsigned int x = 0; x < _templateMaze.size(); x++) {
        for (unsigned int y = 0; y < _templateMaze[x].size(); y++) {
            for (unsigned char i = 0; i < 4; i++) {
                switch (i) {
                    case 0:
                        // Top
                        if (_templateMaze[x][y].walls & 0b1000u)
                            _generatedMaze[x * 3 + 1][y * 3] = 'X';
                        break;
                    case 1:
                        // Right
                        if (_templateMaze[x][y].walls & 0b0100u)
                            _generatedMaze[x * 3 + 2][y * 3 + 1] = 'X';
                        break;
                    case 2:
                        // Down
                        if (_templateMaze[x][y].walls & 0b0010u)
                            _generatedMaze[x * 3 + 1][y * 3 + 2] = 'X';
                        break;
                    case 3:
                        // Left
                        if (_templateMaze[x][y].walls & 0b0001u)
                            _generatedMaze[x * 3][y * 3 + 1] = 'X';
                        break;
                    default:
                        break;
                }
            }
        }
    }


}

void MazeManager::setupTemplate(unsigned int width, unsigned int height) {
    _templateMaze.resize(width);

    for (auto& mazeColumn : _templateMaze) {
        mazeColumn.resize(height);
    }
}

void MazeManager::printMaze() const {
    for (auto& mazeColumn : _generatedMaze) {
        for (auto& mazeElement : mazeColumn) {
            std::cout << mazeElement << ' ';
        }

        std::cout << '\n';
    }
}

void MazeManager::setVisited(Coordinate coordinate) {
    setVisited(coordinate.x, coordinate.y);
}

void MazeManager::setVisited(int x, int y) {
    _templateMaze[x][y].visited = true;
}

bool MazeManager::isInsideMaze(int x, int y) const {
    return !(x < 0 ||
             y < 0 ||
             x >= _templateMaze.size() ||
             y >= _templateMaze[0].size());
}

bool MazeManager::isVisited(int x, int y) const {
    return _templateMaze[x][y].visited;
}

bool MazeManager::isAvailable(int x, int y) const {
    return (isInsideMaze(x, y) && !isVisited(x, y));
}

bool MazeManager::hasUnvisitedNeighbours(Coordinate coordinate) {
    return hasUnvisitedNeighbours(coordinate.x, coordinate.y);
}

bool MazeManager::hasUnvisitedNeighbours(int x, int y) {
    return isAvailable(x + 1, y) ||
           isAvailable(x - 1, y) ||
           isAvailable(x, y + 1) ||
           isAvailable(x, y - 1);
}

void MazeManager::makeHole(Direction direction, Coordinate coordinate) {
    const int x = coordinate.x;
    const int y = coordinate.y;

    switch (direction) {
        case Direction::TOP:
            _templateMaze[x][y].walls |= 0b1000u;
            break;
        case Direction::RIGHT:
            _templateMaze[x][y].walls |= 0b0100u;
            break;
        case Direction::DOWN:
            _templateMaze[x][y].walls |= 0b0010u;
            break;
        case Direction::LEFT:
            _templateMaze[x][y].walls |= 0b0001u;
            break;
        case Direction::NONE:
            throw std::runtime_error("Can't make hole in Direction::NONE\n");
    }
}

void MazeManager::makeHoleBetween(Coordinate cell_1, Coordinate cell_2) {
    const int deltaX = cell_1.x - cell_2.x;
    const int deltaY = cell_1.y - cell_2.y;

    if (deltaX != 0) {
        if (deltaX > 0) {
            makeHole(Direction::LEFT, cell_1);
            makeHole(Direction::RIGHT, cell_2);
        } else {
            makeHole(Direction::RIGHT, cell_1);
            makeHole(Direction::LEFT, cell_2);
        }
    }

    if (deltaY != 0) {
        if (deltaY > 0) {
            makeHole(Direction::TOP, cell_1);
            makeHole(Direction::DOWN, cell_2);
        } else {
            makeHole(Direction::DOWN, cell_1);
            makeHole(Direction::TOP, cell_2);
        }
    }
}

Coordinate MazeManager::getRandomNeighbour(Coordinate coordinate) {
    auto randomGenerator = std::minstd_rand(std::random_device()());
    std::uniform_int_distribution<char> range(0, _directions.size() - 1);

    Coordinate neighbourCell;

    char directionIndex = range(randomGenerator);
    Direction direction = Direction::NONE;

    // Get a randomised direction to pick the neighbour from
    for (int i = 0; i < _directions.size(); i++) {
        if (i == directionIndex) {
            direction = _directions[i];
            _directions.erase(_directions.begin() + i);
        }
    }

    switch (direction) {
        case Direction::TOP:
            neighbourCell.x = coordinate.x;
            neighbourCell.y = coordinate.y - 1;
            break;
        case Direction::RIGHT:
            neighbourCell.x = coordinate.x + 1;
            neighbourCell.y = coordinate.y;
            break;
        case Direction::DOWN:
            neighbourCell.x = coordinate.x;
            neighbourCell.y = coordinate.y + 1;
            break;
        case Direction::LEFT:
            neighbourCell.x = coordinate.x - 1;
            neighbourCell.y = coordinate.y;
            break;
        case Direction::NONE:
            throw std::runtime_error("No remaining direction found when attempting to get random neighbour\n");
        default:
            throw std::runtime_error("Invalid direction received when attempting to get random neighbour\n");
    }

    if (isAvailable(neighbourCell)) {
        resetDirections();
        return neighbourCell;
    } else {
        return getRandomNeighbour(coordinate);
    }
}

void MazeManager::resetDirections() {
    _directions.clear();

    _directions.insert(_directions.end(), {
        Direction::TOP,
        Direction::RIGHT,
        Direction::DOWN,
        Direction::LEFT
    });
}

bool MazeManager::isAvailable(Coordinate coordinate) const {
    return isAvailable(coordinate.x, coordinate.y);
}

std::vector<std::vector<char> > MazeManager::getMaze() const {
    return _generatedMaze;
}

bool MazeManager::isInsideGeneratedMaze(int x, int y) const {
    return x > 0 &&
           y > 0 &&
           x <= _generatedMaze.size() &&
           y <= _generatedMaze[0].size();

}
