#include <iostream>
#include <vector>
#include <SDL2/SDL.h>
#include <cstdint>
#include "Maths.h"
#include "InputManager.h"
#include "WindowManager.h"
#include "MazeManager.h"
#include "MazeGenerator.h"

int main() {
    enum class GameState {
            RUNNING,
            EXIT
    };

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    InputManager inputManager = InputManager();
    WindowManager window = WindowManager("Foobar", 100, 100, 1280, 1024);

    constexpr unsigned char FPS = 20;
    constexpr unsigned char FRAME_DELAY = 1000 / FPS;
    constexpr unsigned int NUM_RAYS = 100; // must not be zero
    constexpr double RAY_INCREMENT = 0.01;
    constexpr double SCALE = 5.0;
    constexpr double MOVE_SPEED = 0.1;
    constexpr double ROTATION_SPEED = 0.025;

    constexpr double FOV = Maths::PI / 8;
    constexpr double RAY_SPACING = FOV / NUM_RAYS;
    constexpr double MAX_DISTANCE = 100;

    double x = 3, y = 3;
    double direction = 0;

    GameState gameState = GameState::RUNNING;

    const int RAY_WIDTH = window.getWidth() / NUM_RAYS;

    MazeManager maze;
    MazeGenerator mazeGenerator(&maze);

    mazeGenerator.generate();
    maze.buildMaze();
    maze.printMaze();

    std::vector<std::vector<char> > map = maze.getMaze();

    std::uint32_t frameStart;
    std::uint8_t frameTime;

    while (gameState == GameState::RUNNING) {
        inputManager.checkInput();

        if (inputManager.shouldQuit()) {
            gameState = GameState::EXIT;
        }

        auto keys = inputManager.getPressedKeys();

        for (const auto &key : *keys) {
            switch (key) {
                case SDLK_s:
                    x -= std::sin(direction + FOV / 2) * MOVE_SPEED;
                    y -= std::cos(direction + FOV / 2)* MOVE_SPEED;
                    break;
                case SDLK_w:
                    x += std::sin(direction + FOV / 2) * MOVE_SPEED;
                    y += std::cos(direction + FOV / 2) * MOVE_SPEED;
                    break;
                case SDLK_d:
                    direction += ROTATION_SPEED;
                    break;
                case SDLK_a:
                    direction -= ROTATION_SPEED;
                    break;
                default:
                    break;
            }
        }

        SDL_SetRenderDrawColor(window.getRenderer(), 0, 0, 0, 255);
        SDL_RenderClear(window.getRenderer());

        frameStart = SDL_GetTicks();

        for (int i = 0; i < NUM_RAYS; i++) {
            float eyeX = x;
            float eyeY = y;
            float distance = 0;

            bool hitWall = false;

            while (!hitWall) {
                const int mapX = static_cast<int>(std::round(eyeX));
                const int mapY = static_cast<int>(std::round(eyeY));

                if (maze.isInsideGeneratedMaze(mapX, mapY) && map[mapX][mapY] != ' ') {
                    window.drawWall(distance, RAY_WIDTH, static_cast<int>(std::round(RAY_WIDTH * i)));
                    hitWall = true;
                } else {
                    eyeX += std::sin(RAY_SPACING * i + direction) * RAY_INCREMENT;
                    eyeY += std::cos(RAY_SPACING * i + direction) * RAY_INCREMENT;

                    distance += RAY_INCREMENT * SCALE;
                }

                if (distance > MAX_DISTANCE) {
                    hitWall = true;
                }
            }
        }

        SDL_RenderPresent(window.getRenderer());

        frameTime = SDL_GetTicks() - frameStart;

        if (FRAME_DELAY > frameTime) {
            SDL_Delay(FRAME_DELAY - frameTime);
        }
    }

    SDL_Quit();
    return 0;
}