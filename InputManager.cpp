//
// Created by neboula on 2019-05-09.
//

#include "InputManager.h"
#include <SDL2/SDL_events.h>

void InputManager::checkInput() {
    SDL_Event evnt;

    while (SDL_PollEvent(&evnt)) {
        switch (evnt.type) {
            case SDL_QUIT:
                _shouldExit = true;
                break;
            case SDL_KEYDOWN:
                _pressedKeys.insert(evnt.key.keysym.sym);
                break;
            case SDL_KEYUP:
                _pressedKeys.erase(evnt.key.keysym.sym);
                break;
            default:
                break;
        }
    }
}

const bool InputManager::shouldQuit() {
    return _shouldExit;
}

const std::set<SDL_Keycode> *InputManager::getPressedKeys() {
    return &_pressedKeys;
}
