//
// Created by newbyte on 4/29/19.
//

#ifndef BACKTRACK_MAZE_MAZEELEMENT_H
#define BACKTRACK_MAZE_MAZEELEMENT_H

struct MazeElement {
    // alright, should probably explain this
    // 1st bit represents top wall
    // 2nd bit represents right wall
    // 3rd bit represents bottom wall
    // 4th bit represents left wall
    unsigned char walls = 0b0000;
    bool visited = false;
};


#endif //BACKTRACK_MAZE_MAZEELEMENT_H
