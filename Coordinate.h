//
// Created by neboula on 2019-05-19.
//

#ifndef BACKTRACK_MAZE_COORDINATE_H
#define BACKTRACK_MAZE_COORDINATE_H

struct Coordinate {
    int x = 0;
    int y = 0;
};

#endif //BACKTRACK_MAZE_COORDINATE_H
