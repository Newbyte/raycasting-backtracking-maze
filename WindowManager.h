//
// Created by neboula on 2019-05-11.
//

#ifndef SDL2_PIXEL_WINDOWMANAGER_H
#define SDL2_PIXEL_WINDOWMANAGER_H

#include <string>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>

class WindowManager {
public:
    WindowManager(const std::string& title, int x, int y, int width, int height);
    ~WindowManager();
    SDL_Renderer* getRenderer();
    const int getWidth() const;
    const int getHeight() const;
    void drawWall(float distance, int width, int x);
private:
    void updateVariables();
    SDL_Window* _window = nullptr;
    SDL_Renderer* _renderer = nullptr;
    int _width = 0;
    int _height = 0;
};


#endif //SDL2_PIXEL_WINDOWMANAGER_H
