//
// Created by neboula on 2019-04-29.
//

#ifndef BACKTRACK_MAZE_DIRECTION_H
#define BACKTRACK_MAZE_DIRECTION_H

enum class Direction {
    TOP,
    RIGHT,
    DOWN,
    LEFT,
    NONE
};

#endif //BACKTRACK_MAZE_DIRECTION_H
