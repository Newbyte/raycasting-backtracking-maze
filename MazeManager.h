//
// Created by neboula on 2019-04-28.
//

#ifndef BACKTRACK_MAZE_MAZEMANAGER_H
#define BACKTRACK_MAZE_MAZEMANAGER_H

#include <vector>
#include <set>
#include "MazeElement.h"
#include "Coordinate.h"
#include "Direction.h"

class MazeManager {
public:
    MazeManager();
    void setupTemplate(unsigned int width, unsigned int height);
    void resetDirections();
    void buildMaze();
    void printMaze() const;
    void makeHole(Direction direction, Coordinate coordinate);
    void makeHoleBetween(Coordinate cell_1, Coordinate cell_2);
    void setVisited(Coordinate coordinate);
    void setVisited(int x, int y);
    bool isInsideMaze(int x, int y) const;
    bool isInsideGeneratedMaze(int x, int y) const;
    bool isVisited(int x, int y) const;
    bool isAvailable(Coordinate coordinate) const;
    bool isAvailable(int x, int y) const;
    bool hasUnvisitedNeighbours(Coordinate coordinate);
    bool hasUnvisitedNeighbours(int x, int y);
    Coordinate getRandomNeighbour(Coordinate coordinate);
    std::vector<std::vector<char> > getMaze() const;
private:
    std::vector<std::vector<MazeElement> > _templateMaze;
    std::vector<std::vector<char> > _generatedMaze;
    std::vector<Direction> _directions;
};


#endif //BACKTRACK_MAZE_MAZEMANAGER_H
